function App() {
	let self = {
		items: [],
		lastCherry: false,
		prefabs: [Cherry],
		background: document.getElementById('bg')
	};

	self.init = () => {
		self.canvas = document.getElementById("canvas");
		self.ctx = self.canvas.getContext("2d");
		self.sizeCanvas();
		// window.requestAnimationFrame(t => self.draw(t));
	}

	self.sizeCanvas = () => {
		self.background.width = window.outerWidth;
        self.background.height = window.outerHeight;
		self.canvas.width = window.innerWidth;
        self.canvas.height = window.innerHeight;
	}

	self.draw = (t) => {
        // window.requestAnimationFrame(t => self.draw(t));
        self.ctx.save();
        self.ctx.clearRect(0, 0, self.canvas.width, self.canvas.height)

        self.ctx.drawImage(self.background, 0, 0, self.background.width, self.background.height); 

        for(let itemKey in self.items) {
        	let item = self.items[itemKey]
        	item.draw(self.ctx);
        }

        self.ctx.restore();
    }

    self.generate = () => {
    	let rand = getRandomArbitrary(0, 10);
    	if(rand == 5) {
    		let Prefab = self.prefabs[Math.floor(Math.random() * self.prefabs.length)];
    		let entity = Prefab({
    			id: Math.random(),
    			x: getRandomArbitrary(0, self.canvas.width),
    			y: getRandomArbitrary(-200, -100)
    		});

    		if(!self.isFalled(entity))
    			self.items.push(entity)
    	}
    }

    self.isFalled = (entity) => {
    	if(entity.isFalled) return true;


    	let offset = entity.y + entity.height / 2;
    	if(offset >= self.canvas.height) return true;

    	// collide
    	for(let itemKey in self.items) {
    		let item = self.items[itemKey];
    		if(item.id != entity.id) {
	    		var hit = !(entity.x + entity.width / 2 < item.x ||
							item.x + item.width / 2 < entity.x ||
							entity.y + entity.height / 2 < item.y ||
							item.y + item.height / 2 < entity.y);

	    		// console.log(hit);

	    		if(hit) return true;
	    	}
    	}

    	return false;
    }

    self.handlers = () => {
    	window.addEventListener("resize", self.sizeCanvas)

        let ts;
    	self.canvas.addEventListener("touchstart", (e) => {
            ts = e.touches[0].clientY;
        })
        self.canvas.addEventListener("touchmove", (e) => {
            let te = e.changedTouches[0].clientY;
            if (ts > te) {
                self.onWheel({wheelDelta: -15})
            }
        })

        self.canvas.addEventListener("wheel", self.onWheel)
    }

    self.onWheel = (e) => {
        self.draw();
    	if(e.wheelDelta < 0) {
    		self.generate();
			for(let itemKey in self.items) {
	        	let item = self.items[itemKey]
	        	item.fall(10, self.isFalled);
	        }
		}
    }

	self.init()
	self.handlers()
	return self;
}

const app = App();

function getRandomArbitrary(min, max) {
  return Math.floor(Math.random() * (max - min) + min);
}

// setInterval(() => app.onWheel({wheelDelta: -15}), 10)