function Cherry({id, x, y}) {
	let self = {
		id, x, y,
		width: 101,
		height: 105,
		isFalled: false,
		image: document.getElementById('cherry')
	}

	self.draw = (ctx) => {
		ctx.drawImage(self.image, self.x - self.width / 2, self.y - self.height / 2);
	}

	self.fall = (delta, isFalled) => {
		if(!self.isFalled) {
			self.y += delta;
			self.isFalled = isFalled(self);
		}
	}

	return self;
}